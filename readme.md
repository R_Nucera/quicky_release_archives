# Quicky release archive

Questo repository pubblico nasce allo scopo di rendere disponibili dei link pubblici relativi alle release di progetti Quicky, create sotto forma di archivi criptati, da utilizzare all'interno di script di bulk update oppure all'interno di immagini SD per i vari prodotti.